package jp.alhinc.ryo_takenaka.calculate_sales;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CalculateSales {

	public static void main(String[] args) {
		List<String> list = new ArrayList<>();  //branch.lst読み込み用
		HashMap<String, String> map = new HashMap<String, String>();
		HashMap<String, Long> map2 = new HashMap<String, Long>();
		BufferedReader br = null;
		File file = null;
		String path;
		CalculateSales calcu = new CalculateSales();

		try{
			if(args[0] == null){
				System.out.println("予期せぬエラーが発生しました");
				return;
			}

			if(args.length >= 2){
				System.out.println("予期せぬエラーが発生しました");
				return;
			}

			path = args[0];
			file = new File(path, "branch.lst");
			if(file.exists()){
				br = new BufferedReader
						(new FileReader(file));
			}else{
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			boolean input;
			input = calcu.fileInput(br, list);
			if(input == false){
				return;
			}

		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally{
			if(br != null){
				try{
				   br.close();
			    }catch(IOException e){
				   System.out.println("予期せぬエラーが発生しました");
				   return;
			    }
			}
		}

		List<String> sales = new ArrayList<>();  //売り上げ結果読み込み用
		List<String> rcdFile = new ArrayList<>();
		String[] idnum2 = new String[list.size()];  //支店ID
		String[] name = new String[list.size()];  //支店名
		long[] calculate = new long[list.size()];  //売り上げ
		String id;
		String line2;
		String salesList;
		BufferedReader br2 = null;

		for(int i = 0; i < list.size(); i++){  //支店名とidの抽出
		   	id = list.get(i);
		   	String[] idnum = id.split("\\,");
		   	idnum2[i] = idnum[0];
		   	name[i] = idnum[1];
		   	map.put(idnum2[i], name[i]);
		}

		try{
			File file3 = new File(args[0]);
			File[] files = file3.listFiles();

			for(int i = 0; i < files.length; i++){
				salesList = files[i].getName();

				if(files[i].isFile() && salesList.matches("^[0-9]{8}.rcd$")){
					rcdFile.add(salesList);
				}
			}

			int rcdName = 0;
			int rcdCount = 1;
			File file2;
			//連番チェック
			for(int i = 0; i < rcdFile.size() -1; i++){
				String rcd1 = rcdFile.get(i).substring(0, 8);
				String rcd2 = rcdFile.get(rcdCount).substring(0, 8);
				int rcdNum1 = Integer.parseInt(rcd1);
				int rcdNum2 = Integer.parseInt(rcd2);
				if((rcdNum2 - rcdNum1) != 1){
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
				rcdCount++;
			}

			for(int i = 0; i < rcdFile.size(); i++){
				int lineCount = 0;
				file2 = new File(rcdFile.get(i));
				br2 = new BufferedReader
						(new FileReader(file2));

				while((line2 = br2.readLine()) != null){
					if(lineCount < 2){
						sales.add(line2);
					}else{
						System.out.println(rcdFile.get(rcdName) + "のフォーマットが不正です");
						return;
					}

					//売上金額が数値かチェック
					if(lineCount == 1){
						if(!line2.matches("^[0-9]*$")){
							System.out.println("予期せぬエラーが発生しました");
							return;
						}
					}

					//支店コードチェック
					if(lineCount == 0){
						if(map.get(line2) == null){
							System.out.println(rcdFile.get(rcdName) + "の支店コードが不正です");
							return;
						}
					}
					lineCount++;
				}
				//行数チェック（1行以下の場合）
				if(lineCount <= 1){
					System.out.println(rcdFile.get(rcdName) + "のフォーマットが不正です");
					return;
				}
				rcdName++;
			}
		}catch(IOException e){
				System.out.println("予期せぬエラーが発生しました");
				return;
		}finally{
			if(br2 != null){
				try{
					br2.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		long[] sale = new long[sales.size()/2];
		String[] saleid = new String[sales.size()/2];
		int count = 0;
		int count2 = 0;
		for(int i = 0; i < sales.size(); i++){
			if(i % 2 == 0){
				saleid[count] = sales.get(i);
				count++;
			}else if(i % 2 == 1){
				sale[count2] = Long.parseLong(sales.get(i));
				count2++;
			}
		}

		//売上集計
		for(int i = 0; i < calculate.length; i++){
			for (int j = 0; j < sale.length; j++){
				try{
					if(idnum2[i].equals(saleid[j])){
						calculate[i] += sale[j];
						if(calculate[i] > 9999999999L){
							System.out.println("合計金額が10桁を超えました");
							return;
						}
					}
				}catch(Exception e){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		List<String> idlist = new ArrayList<>();
		for(String str : saleid){
			idlist.add(str);
		}

		for(int i = 0; i < idnum2.length; i++){
			map2.put(idnum2[i], calculate[i]);
		}

		for(int i = 0; i < map2.size(); i++){
			if(map2.get(idnum2[i]) == null)
				map2.put(idnum2[i], 0L);
		}

		boolean output;
		try{
			output = calcu.fileOutput(path, idnum2, list, map2);
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		if(output == false){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}


	//branch.lstの読み込み
	public boolean fileInput(BufferedReader br, List<String> list) throws IOException{
		String line;
		while((line = br.readLine()) != null){
			int specificWord = line.indexOf(",");
			int codeCount = 2;
			int codeCount2 = 4 ;
			//支店コードの桁数チェック
			if(specificWord == 3){
				list.add(line);
				String[] str = line.split("\\,");
				String code = str[0];
				if(!code.matches("^[0-9]*$")){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}

				//要素数チェック
				if(str.length <= 1 || str.length >= 3){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
			}else if(specificWord < codeCount || specificWord >= codeCount2){
				System.out.println("支店定義ファイルのフォーマットが不正です");
				return false;
			}
		}
		return true;
	}


	//branch.outの出力
	public boolean fileOutput(String path, String[] idnum2, List<String> list
										, HashMap<String, Long> map2) throws IOException{
		BufferedWriter bw = null;
		PrintWriter pw = null;

		File file4 = new File(path, "branch.out");
		FileWriter fw = new FileWriter(file4);
		bw = new BufferedWriter(fw);
		pw = new PrintWriter(bw);
		for(int i = 0; i < list.size(); i++){
			pw.println(list.get(i) + "," + map2.get(idnum2[i]));
		}
		if(pw != null){
			try{
				pw.close();
			}catch(Exception e){
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
		return true;
	}
}




